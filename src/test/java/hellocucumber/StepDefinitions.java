package hellocucumber;

import io.cucumber.java.Before;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static org.junit.Assert.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;


public class StepDefinitions {
    private static WebDriver driver;
    private static String jiraUrl = System.getenv("JIRA_URL");

    @Before()
    public void openBrowser() {
        // Set Chrome Path
        String chromePath = "chromedriver83-mac";
        System.setProperty("webdriver.chrome.driver", chromePath);

        // Start Browser
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Given("Users visit Jira web page")
    public void users_visit_email() throws InterruptedException {
        driver.get(jiraUrl);
        TimeUnit.SECONDS.sleep(1);
    }

    @When("Users enter {string} in username field")
    public void users_enter_username(String username) throws InterruptedException {
        WebElement usernameField = driver.findElement(By.name("os_username"));
        // Enter username in username field
        usernameField.sendKeys(username);
        TimeUnit.SECONDS.sleep(1);
    }

    @When("Users enter {string} in password field")
    public void users_enter_password(String password) throws InterruptedException {
        WebElement passwordField = driver.findElement(By.name("os_password"));
        // Enter password in password field
        passwordField.sendKeys(password);
        TimeUnit.SECONDS.sleep(1);
    }

    @When("Users press Submit button")
    public void users_press_submit() throws InterruptedException {
        WebElement submitButton = driver.findElement(By.name("login"));
        // Click Submit button
        submitButton.click();
        TimeUnit.SECONDS.sleep(1);
    }

    @Then("Users can see {string}")
    public void users_see_name(String element) {
        WebElement navItem = driver.findElement(By.id("home_link"));
        System.out.println(navItem.getText());
        assertEquals(element, navItem.getText());
    }



    @After()
    public void closeBrowser() {
        driver.quit();
    }
}
