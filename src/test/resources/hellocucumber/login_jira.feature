Feature: Jira Login Feature

	Scenario Outline: Login Jira Dashboard Successfully
		Given Users visit Jira web page
		When Users enter "<username>" in username field
		And Users enter "<password>" in password field
		And Users press Submit button
		Then Users can see "<element>"

		Examples:
			| username | password  | element    |
			| admin    | admin     | Dashboards |